# N-queens
Implementation of the N-queen problem is taken from the [example](https://github.com/utwente-fmt/sylvan/blob/master/examples/nqueens.c) provided by [Sylvan](https://github.com/utwente-fmt/sylvan).  

The repository is self-contained---it contains all the libraries and *should* be easy to build using `cmake`.

```sh
# you need to build cudd manually first
cd cudd
autoreconf -f -i
./configure --enable-obj
make -j4 check
cd ..

# then
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j4
cd ..
./run-all.sh
```