#include <libteddy/details/tools.hpp>
#include <libteddy/details/pla_file.hpp>

#include <filesystem>
#include <iostream>
#include <string_view>

#include "buddy.hpp"
#include "cudd.hpp"
#include "common.hpp"
#include "sylvan.hpp"
#include "teddy.hpp"

struct BenchmarkConfig
{
    int warmupReplications_;
    int replicationCount_;
    std::string_view csvRecordSep_;
    std::string_view csvLineSep_;
};

int n_queen_benchmark(
    int argc,
    char** argv,
    BenchmarkConfig const config,
    NQueensResult(*solver)(std::size_t)
)
{
    if (argc < 2)
    {
        std::cerr << "Please specify the number of queens.\n";
        return 2;
    }

    std::optional<std::size_t> nOpt = teddy::utils::parse<std::size_t>(argv[1]);
    if (not nOpt)
    {
        std::cerr << "The number of queens must be a natural number.\n";
        return 2;
    }

    auto const Sep = ";";
    auto const Eol = "\n";

    std::size_t const n = *nOpt;

    std::cout
        << "rep-id"
        << Sep
        << "n-queens"
        << Sep
        << "total-duration[ms]"
        << Sep
        << "sat-count-duration[ns]"
        << Sep
        << "solution-count"
        << Eol;

    using ms = ch::milliseconds;
    using ns = ch::nanoseconds;

    for (int i = 0; i < config.replicationCount_; ++i)
    {
        NQueensResult const result = solver(n);
        std::cout
            << i
            << config.csvRecordSep_
            << n
            << config.csvRecordSep_
            << ch::duration_cast<ms>(result.totalDuration_).count()
            << config.csvRecordSep_
            << ch::duration_cast<ns>(result.satcountDuration_).count()
            << config.csvRecordSep_
            << result.solutionCount_
            << config.csvLineSep_;
    }
    return 0;
}

int pla_benchmark(
    int argc,
    char** argv,
    BenchmarkConfig const config,
    CircuitResult(*solver)(teddy::pla_file const&)
)
{
    if (argc < 2)
    {
        std::cerr << "Please specify name of the PLA file.\n";
        return 2;
    }

    std::optional<teddy::pla_file> file = teddy::pla_file::load_file(argv[1]);
    if (not file)
    {
        std::cerr << "Failed to load " << argv[1] << ".\n";
        return 2;
    }

    auto const fileName = std::filesystem::path(argv[1]).filename().native();

    using ms = ch::milliseconds;
    using ns = ch::nanoseconds;

    std::cout
        << "file-name"
        << config.csvRecordSep_
        << "rep-id"
        << config.csvRecordSep_
        << "sf-diagram-size"
        << config.csvRecordSep_
        << "avg-truth-density"
        << config.csvRecordSep_
        << "availability"
        << config.csvRecordSep_
        << "create-duration[ns]"
        << config.csvRecordSep_
        << "avg-t-density-duration[ns]"
        << config.csvRecordSep_
        << "avg-availability-duration[ns]"
        << config.csvLineSep_;

    for (int i = 0; i < config.warmupReplications_; ++i)
    {
        CircuitResult volatile const result = solver(*file);
    }

    for (int i = 0; i < config.replicationCount_; ++i)
    {
        CircuitResult const result = solver(*file);
        std::cout
            << fileName
            << config.csvRecordSep_
            << i
            << config.csvRecordSep_
            << result.sfDiagramSize_
            << config.csvRecordSep_
            << result.avgTruthDensity_
            << config.csvRecordSep_
            << result.availability_
            << config.csvRecordSep_
            << ch::duration_cast<ns>(result.createDuration_).count()
            << config.csvRecordSep_
            << ch::duration_cast<ns>(result.avgTDensityDuration_).count()
            << config.csvRecordSep_
            << ch::duration_cast<ns>(result.avgAvailabilityDuration_).count()
            << config.csvLineSep_;
    }

    return 0;
}

int main(int argc, char** argv)
{
    NQueensResult(*n_queen_solver)(std::size_t) = nullptr;
    CircuitResult(*circuit_solver)(teddy::pla_file const&) = nullptr;

    if (argc < 3)
    {
        std::cerr << "Usage:\n";
        std::cerr << "./run (buddy|cudd|sylvan|teddy) (nqueen|pla) [args]\n";
        return 1;
    }

    // Select library
    std::string_view library = argv[1];
    if (library == "buddy")
    {
        n_queen_solver = b_buddy::solve_n_queens;
        circuit_solver = b_buddy::solve_circuit;
    }
    else if (library == "cudd")
    {
        n_queen_solver = b_cudd::solve_n_queens;
        circuit_solver = b_cudd::solve_circuit;
    }
    else if (library == "sylvan")
    {
        n_queen_solver = b_sylvan::solve_n_queens;
        circuit_solver = b_sylvan::solve_circuit;
    }
    else if (library == "teddy")
    {
        n_queen_solver = b_teddy::solve_n_queens;
        circuit_solver = b_teddy::solve_circuit;
    }
    else
    {
        std::cerr << "Unknown library " << library << "\n.";
        return 1;
    }

    // Config benchmark
    BenchmarkConfig config
    {
        .warmupReplications_ = 10,
        .replicationCount_   = 100,
        .csvRecordSep_       = "\t",
        .csvLineSep_         = "\n"
    };

    // Select benchmark
    std::string_view benchmark = argv[2];
    if (benchmark == "nqueen")
    {
        return n_queen_benchmark(
            argc - 2,
            argv + 2,
            config,
            n_queen_solver
        );
    }
    else if (benchmark == "pla")
    {
        return pla_benchmark(
            argc - 2,
            argv + 2,
            config,
            circuit_solver
        );
    }
    else
    {
        std::cerr << "Unknown benchmark " << benchmark << ".\n";
        return 1;
    }
}


// truth-density podla definicie pre mensie PLA (posutpne fixovat vsetky vstupy), vysledok bude priemerny cas
// truth-density podla satcountln|pravdepodobnostnym algoritmom pre vacsie PLA (posutpne fixovat vsetky vstupy)
// (iba teddy) availability

// Smaller PLAs:
//   con1.pla
//   xor5.pla
//   rd53.pla
//   squar5.pla
//   sqrt8.pla

// Larger PLAs:
//   ex1010.pla
//   alu4.pla
//   misex3.pla
//   spla.pla
//   pdc.pla