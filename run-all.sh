#!/bin/bash

QUEEN_COUNT=8
REPLICATION_COUNT=10

echo -n "TeDDy;"
echo -n "BuDDy;"
echo -n "CUDD;"
echo -n "Sylvan(all-cores);"
echo -n "Sylvan(single-core)"
echo ""

for ((i=0; i <= $REPLICATION_COUNT; i++)); do
    ELAPSED_TEDDY=$(./build/run-teddy ${QUEEN_COUNT})
    ELAPSED_BUDDY=$(./build/run-buddy ${QUEEN_COUNT})
    ELAPSED_CUDD=$(./build/run-cudd ${QUEEN_COUNT})
    ELAPSED_SYLVAN_PARALLEL=$(./build/run-sylvan -w0 ${QUEEN_COUNT})
    ELAPSED_SYLVAN_SINGLE=$(./build/run-sylvan -w1 ${QUEEN_COUNT})
    echo -n "${ELAPSED_TEDDY};"
    echo -n "${ELAPSED_BUDDY};"
    echo -n "${ELAPSED_CUDD};"
    echo -n "${ELAPSED_SYLVAN_SINGLE};"
    echo -n "${ELAPSED_SYLVAN_PARALLEL}"
    echo ""
done