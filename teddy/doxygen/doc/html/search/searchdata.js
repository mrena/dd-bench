var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvx~",
  1: "abcdefghilmnoprtuvx",
  2: "o",
  3: "abcdefgilmorstuv~",
  4: "d",
  5: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "typedefs",
  5: "concepts"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Typedefs",
  5: "Concepts"
};

