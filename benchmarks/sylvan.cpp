#include <libteddy/details/pla_file.hpp>
#include <libteddy/details/tools.hpp>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <iostream>

#include <optional>
#include <sylvan.h>
#include <sylvan_obj.hpp>

#include "common.hpp"

using namespace sylvan;

namespace b_sylvan
{

NQueensResult solve_n_queens(std::size_t const n)
{
    long long solutionCount          = -1;
    ch::nanoseconds totalDuration    = ch::nanoseconds::zero();
    ch::nanoseconds satcountDuration = ch::nanoseconds::zero();

    int n_workers = 0; // automatically detect number of workers
    char* envWorkers = std::getenv("SYLVAN_N_WORKERS");
    if (envWorkers)
    {
        std::optional<int> nWorkersOpt = teddy::utils::parse<int>(envWorkers);
        if (nWorkersOpt)
        {
            n_workers = *nWorkersOpt;
        }
        else
        {
            std::cerr << "Failed to parse SYLVAN_N_WORKERS '"
                      << envWorkers
                      << "' using default.\n";
        }
    }

    auto const tick = ch::high_resolution_clock::now();
    {
    lace_start(n_workers, 1000000); // Initialize the Lace framework for <n_workers> workers.

    sylvan_set_sizes(1LL<<20, 1LL<<24, 1LL<<18, 1LL<<22);
    sylvan_init_package();
    sylvan_set_granularity(3);
    sylvan_init_bdd();

    Bdd zero = Bdd::bddZero();
    Bdd one = Bdd::bddOne();

    std::vector<Bdd> board;
    board.reserve(n*n);
    for (std::size_t i = 0; i < n*n; i++)
    {
        board.push_back(Bdd::bddVar(i));
    }

    Bdd res = one, temp = one;

    // Old satcount function still requires a silly variables cube
    Bdd vars = one;
    for (std::size_t i = 0; i < n*n; i++)
    {
        vars &= board[i];
    }

    // rows
    for (std::size_t i = 0; i < n; i++)
    {
        for (std::size_t j = 0; j < n; j++)
        {
            temp = one;
            for (std::size_t k = 0; k < n; k++)
            {
                if (j != k)
                {
                    temp &= !board[i*n + k];
                }
            }
            temp |= !board[i*n + j];
            res &= temp;
        }
    }

    // cols
    for (std::size_t j = 0; j < n; j++)
    {
        for (std::size_t i = 0; i < n; i++)
        {
            temp = one;
            for (std::size_t k = 0; k < n; k++)
            {
                if (i != k)
                {
                    temp &= !board[k*n + j];
                }
            }
            temp |= !board[i*n + j];
            res &= temp;
        }
    }

    // rising diagonals
    for (std::size_t i = 0; i < n; i++)
    {
        for (std::size_t j = 0; j < n; j++)
        {
            temp = one;
            for (std::size_t k = 0; k < n; k++)
            {
                if (j+k >= i && j+k < n+i && k != i)
                {
                    temp &= !board[k*n + (j+k-i)];
                }
            }
            temp |= !board[i*n + j];
            res &= temp;
        }
    }

    // falling diagonals
    for (std::size_t i = 0; i < n; i++)
    {
        for (std::size_t j = 0; j < n; j++)
        {
            temp = one;
            for (std::size_t k = 0; k < n; k++)
            {
                if (j+i >= k && j+i < n+k && k != i)
                {
                    temp &= !board[k*n + (j+i-k)];
                }
            }
            temp |= !board[i*n + j];
            res &= temp;
        }
    }

    // place queens
    for (std::size_t i = 0; i<n; i++)
    {
        temp = zero;
        for (std::size_t j = 0; j<n; j++)
        {
            temp |= board[i*n + j];
        }
        res &= temp;
    }

    auto const tickSat = ch::high_resolution_clock::now();
    solutionCount      = solutionCount = static_cast<long long>(
        res.SatCount(BddSet(vars))
    );
    auto const tockSat = ch::high_resolution_clock::now();
    satcountDuration   = ch::duration_cast<ch::nanoseconds>(tockSat - tickSat);

    sylvan_quit();
    lace_stop();
    }
    auto const tock = ch::high_resolution_clock::now();

    totalDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);

    return NQueensResult
    {
        .solutionCount_    = static_cast<long long>(solutionCount),
        .totalDuration_    = totalDuration,
        .satcountDuration_ = satcountDuration
    };
}

CircuitResult solve_circuit(teddy::pla_file const& file)
{
    // TODO move to function
    int n_workers = 0; // automatically detect number of workers
    char* envWorkers = std::getenv("SYLVAN_N_WORKERS");
    if (envWorkers)
    {
        std::optional<int> nWorkersOpt = teddy::utils::parse<int>(envWorkers);
        if (nWorkersOpt)
        {
            n_workers = *nWorkersOpt;
        }
        else
        {
            std::cerr << "Failed to parse SYLVAN_N_WORKERS '"
                      << envWorkers
                      << "' using default.\n";
        }
    }

    auto const& plaLines          = file.get_lines();
    long long const inputCount    = file.get_variable_count();
    long long const lineCount     = file.get_line_count();
    long long const functionCount = file.get_function_count();
    long long const totalVarCount = inputCount + lineCount + functionCount;

    lace_start(n_workers, 1000000);
    sylvan_set_sizes(1LL<<20, 1LL<<24, 1LL<<18, 1LL<<22);
    sylvan_init_package();
    sylvan_set_granularity(3);
    sylvan_init_bdd();

    auto tick = std::chrono::high_resolution_clock::now();
    auto tock = tick;

    std::vector<Bdd> inputs;
    std::vector<Bdd> andGates;
    std::vector<Bdd> orGates;

    int i = 0;
    // Variables for inputs
    for (int k = 0; k < inputCount; ++k)
    {
        inputs.push_back(Bdd::bddVar(i++));
    }

    // Variables for and gates
    for (int k = 0; k < lineCount; ++k)
    {
        andGates.push_back(Bdd::bddVar(i++));
    }

    // Variables for or gates
    for (int k = 0; k < functionCount; ++k)
    {
        orGates.push_back(Bdd::bddVar(i++));
    }

    int nextOrVar = 0;
    Bdd failedAnd = Bdd::bddZero();
    Bdd failedOr  = Bdd::bddOne();

    // BDDs for output functions
    std::vector<Bdd> functions;
    // BDDs for output functions including unreliable gates
    std::vector<Bdd> functionsRel;
    functions.reserve(functionCount);
    functionsRel.reserve(functionCount);

    for (int fi = 0; fi < functionCount; ++fi)
    {
        int nextAndVar = -1;
        std::vector<Bdd> products;
        std::vector<Bdd> productsRel;
        products.reserve(lineCount);
        productsRel.reserve(lineCount);
        for (int li = 0; li < lineCount; ++li)
        {
            ++nextAndVar;
            if (plaLines[li].fVals_.get(fi) != 1)
            {
                continue;
            }

            teddy::bool_cube const& cube = plaLines[li].cube_;
            Bdd product = Bdd::bddOne();
            for (int i = 0; i < cube.size(); ++i)
            {
                if (cube.get(i) == 1)
                {
                    product *= inputs[i];
                }
                else if (cube.get(i) == 0)
                {
                    product *= !inputs[i];
                }
            }

            Bdd productRel =
                (andGates[nextAndVar] * product) +
                (!andGates[nextAndVar] * failedAnd);

            products.push_back(product);
            productsRel.push_back(productRel);
        }

        if (products.empty())
        {
            products.push_back(Bdd::bddZero());
        }

        Bdd sum = Bdd::bddZero();
        for (Bdd const& product : products)
        {
            sum += product;
        }

        Bdd sumRel =
            (orGates[nextOrVar] * sum) +
            (!orGates[nextOrVar] * failedOr);
        ++nextOrVar;

        functions.push_back(sum);
        functionsRel.push_back(sumRel);
    }

    // BDD for output structure functions
    std::vector<Bdd> structureFunctions;
    structureFunctions.reserve(functionCount);
    for (std::size_t i = 0; i < functionCount; ++i)
    {
        structureFunctions.push_back(
            functions[i].Xnor(functionsRel[i])
        );
    }

    // BDD for the final PLA structure function
    Bdd structureFunction = Bdd::bddOne();
    for (Bdd const& dd : structureFunctions)
    {
        structureFunction *= dd;
    }

    tock = std::chrono::high_resolution_clock::now();
    auto const createDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    long long const sfDiagramSize = structureFunction.NodeCount();


    Bdd xVars = Bdd::bddOne();
    for (int i = 0; i < inputCount; ++i)
    {
        xVars &= inputs[i];
    }

    for (int i = 0; i < lineCount; ++i)
    {
        xVars &= andGates[i];
    }

    for (int i = 0; i < functionCount; ++i)
    {
        xVars &= orGates[i];
    }

    ch::nanoseconds totalTDDuration = ch::nanoseconds::zero();
    double totalTruthDensity        = 0;

    // Calculation of truth density for each input combination
    for_each_bdd_vars(inputCount, [&](auto const& vars)
    {
        Bdd mask = Bdd::bddOne();
        for (int i = 0; i < inputCount; ++i)
        {
            if (vars[i])
            {
                mask &= inputs[i];
            }
            else
            {
                mask &= !inputs[i];
            }
        }

        Bdd tmpSf = structureFunction.Restrict(mask);

        double truthDensity = -1;
        tick = std::chrono::high_resolution_clock::now();
        if (totalVarCount < 53)
        {
            truthDensity = tmpSf.SatCount(xVars)
                         / static_cast<double>(1LL << (totalVarCount));
        }
        else
        {
            // TODO ako?
            // double const lga = bdd_satcountln(tmpSf);
            // double const lgb = totalVarCount;
            // truthDensity     = std::pow(2, lga - lgb);
        }
        tock = std::chrono::high_resolution_clock::now();
        totalTDDuration += ch::duration_cast<ch::nanoseconds>(tock - tick);
        totalTruthDensity += truthDensity;
    });

    sylvan_quit();
    lace_stop();

    double const avgTruthDensity = totalTruthDensity / (1LL << inputCount);
    ch::nanoseconds const avgTDDuration = totalTDDuration / (1LL << inputCount);

    return CircuitResult
    {
        .sfDiagramSize_           = sfDiagramSize,
        .avgTruthDensity_         = avgTruthDensity,
        .availability_            = -1,
        .createDuration_          = createDuration,
        .avgTDensityDuration_     = avgTDDuration,
        .avgAvailabilityDuration_ = ch::nanoseconds::zero()
    };
}

}