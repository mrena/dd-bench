#include "teddy.hpp"

#include <libteddy/reliability.hpp>
#include <libteddy/details/tools.hpp>
#include <array>
#include <chrono>
#include <optional>
#include <random>
#include <vector>

#include "common.hpp"

namespace b_teddy
{

struct UniformProbProxy
{
    double operator[](int) const
    {
        return 0.5;
    }
};

struct UniformProb
{
    UniformProbProxy operator[](int) const
    {
        return UniformProbProxy();
    }
};

std::vector<std::array<double, 2>> generate_probabilities(int const varCount)
{
    std::ranlux48 rng(144);
    std::uniform_real_distribution<double> dist(0.0, 1.0);
    std::vector<std::array<double, 2>> ps(varCount);
    for (int i = 0; i < varCount; ++i)
    {
        ps[i][1] = dist(rng);
        ps[i][0] = 1 - ps[i][1];
    }
    return ps;
}

NQueensResult solve_n_queens(std::size_t const n)
{
    long long solutionCount          = -1;
    ch::nanoseconds totalDuration    = ch::nanoseconds::zero();
    ch::nanoseconds satcountDuration = ch::nanoseconds::zero();

    auto const tick = ch::high_resolution_clock::now();
    {
    teddy::bss_manager manager(n*n, 5'000'000);
    manager.set_cache_ratio(2);
    manager.set_gc_ratio(0.50);
    using bdd_t = teddy::bss_manager::diagram_t;
    using namespace teddy::ops;

    std::vector<bdd_t> board;
    board.reserve(n*n);
    for (std::size_t i = 0; i < n*n; ++i)
    {
        board.push_back(manager.variable(i));
    }

    bdd_t result = manager.constant(1);

    // rows
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            bdd_t tmp = manager.constant(1);
            for (std::size_t k = 0; k < n; ++k){
                if (j != k)
                {
                    tmp = manager.apply<AND>(
                        tmp,
                        manager.negate(board[i*n + k])
                    );
                }
            }
            tmp = manager.apply<OR>(
                tmp,
                manager.negate(board[i*n + j])
            );
            result = manager.apply<AND>(result, tmp);
        }
    }

    // cols
    for (std::size_t j = 0; j < n; ++j)
    {
        for (std::size_t i = 0; i < n; ++i)
        {
            bdd_t tmp = manager.constant(1);
            for (std::size_t k = 0; k < n; ++k)
            {
                if (i != k)
                {
                    tmp = manager.apply<AND>(
                        tmp,
                        manager.negate(board[k*n + j])
                    );
                }
            }
            tmp = manager.apply<OR>(
                tmp,
                manager.negate(board[i*n + j])
            );
            result = manager.apply<AND>(result, tmp);
        }
    }

    // rising diagonals
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            bdd_t tmp = manager.constant(1);
            for (std::size_t k = 0; k < n; ++k)
            {
                if (j+k >= i && j+k < n+i && k != i)
                {
                    tmp = manager.apply<AND>(
                        tmp,
                        manager.negate(board[k*n + (j+k-i)])
                    );
                }
            }
            tmp = manager.apply<OR>(
                tmp,
                manager.negate(board[i*n + j])
            );
            result = manager.apply<AND>(result, tmp);
        }
    }

    // falling diagonals
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            bdd_t tmp = manager.constant(1);
            for (std::size_t k = 0; k < n; ++k)
            {
                if (j+i >= k && j+i < n+k && k != i)
                {
                    tmp = manager.apply<AND>(tmp, manager.negate(board[k*n + (j+i-k)]));
                }
            }
            tmp = manager.apply<OR>(tmp, manager.negate(board[i*n + j]));
            result = manager.apply<AND>(result, tmp);
        }
    }

    // place queens
    for (std::size_t i = 0; i < n; ++i) {
        bdd_t tmp = manager.constant(0);
        for (std::size_t j = 0; j < n; ++j) {
            tmp = manager.apply<OR>(tmp, board[i*n + j]);
        }
        result = manager.apply<AND>(result, tmp);
    }

    auto const tickSat = ch::high_resolution_clock::now();
    solutionCount      = manager.satisfy_count(1, result);
    auto const tockSat = ch::high_resolution_clock::now();
    satcountDuration   = ch::duration_cast<ch::nanoseconds>(tockSat - tickSat);
    }
    auto const tock = ch::high_resolution_clock::now();

    totalDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    return NQueensResult
    {
        .solutionCount_    = static_cast<long long>(solutionCount),
        .totalDuration_    = totalDuration,
        .satcountDuration_ = satcountDuration
    };
}

CircuitResult solve_circuit(teddy::pla_file const& file)
{
    auto const& plaLines          = file.get_lines();
    long long const inputCount    = file.get_variable_count();
    long long const lineCount     = file.get_line_count();
    long long const functionCount = file.get_function_count();
    long long const totalVarCount = inputCount + lineCount + functionCount;

    teddy::bss_manager manager(totalVarCount, 1'000'000);

    using namespace teddy::ops;
    using bdd = teddy::bss_manager::diagram_t;

    auto tick = std::chrono::high_resolution_clock::now();
    auto tock = tick;

    std::vector<bdd> inputs;
    std::vector<bdd> andGates;
    std::vector<bdd> orGates;

    int i = 0;
    // Variables for inputs
    for (int k = 0; k < inputCount; ++k)
    {
        inputs.push_back(manager.variable(i++));
    }

    // Variables for and gates
    for (int k = 0; k < lineCount; ++k)
    {
        andGates.push_back(manager.variable(i++));
    }

    // Variables for or gates
    for (int k = 0; k < functionCount; ++k)
    {
        orGates.push_back(manager.variable(i++));
    }

    int nextOrVar = 0;
    bdd failedAnd = manager.constant(0);
    bdd failedOr  = manager.constant(1);

    // BDDs for output functions
    std::vector<bdd> functions;
    // BDDs for output functions including unreliable gates
    std::vector<bdd> functionsRel;
    functions.reserve(functionCount);
    functionsRel.reserve(functionCount);

    for (int fi = 0; fi < functionCount; ++fi)
    {
        int nextAndVar = -1;
        std::vector<bdd> products;
        std::vector<bdd> productsRel;
        products.reserve(lineCount);
        productsRel.reserve(lineCount);

        for (int li = 0; li < lineCount; ++li)
        {
            ++nextAndVar;
            if (plaLines[li].fVals_.get(fi) != 1)
            {
                continue;
            }

            teddy::bool_cube const& cube = plaLines[li].cube_;
            bdd product = manager.constant(1);
            for (int i = 0; i < cube.size(); ++i)
            {
                if (cube.get(i) == 1)
                {
                    product = manager.apply<AND>(
                        product,
                        inputs[i]
                    );
                }
                else if (cube.get(i) == 0)
                {
                    product = manager.apply<AND>(
                        product,
                        manager.negate(inputs[i])
                    );
                }
            }

            bdd productRel = manager.apply<OR>(
                manager.apply<AND>(andGates[nextAndVar], product),
                manager.apply<AND>(manager.negate(andGates[nextAndVar]), failedAnd)
            );

            products.push_back(product);
            productsRel.push_back(productRel);
        }

        if (products.empty())
        {
            products.push_back(manager.constant(0));
        }

        bdd sum = manager.constant(0);
        for (bdd const& product : products)
        {
            sum = manager.apply<OR>(sum, product);
        }

        bdd sumRel = manager.apply<OR>(
            manager.apply<AND>(orGates[nextOrVar], sum),
            manager.apply<AND>(manager.negate(orGates[nextOrVar]), failedOr)
        );
        ++nextOrVar;

        functions.push_back(sum);
        functionsRel.push_back(sumRel);
    }

    // BDD for output structure functions
    std::vector<bdd> structureFunctions;
    structureFunctions.reserve(functionCount);
    for (std::size_t i = 0; i < functionCount; ++i)
    {
        structureFunctions.push_back(
            manager.apply<EQUAL_TO>(functions[i], functionsRel[i])
        );
    }

    bdd structureFunction = manager.tree_fold<AND>(structureFunctions);

    tock = std::chrono::high_resolution_clock::now();
    auto const createDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    long long const sfDiagramSize = manager.get_node_count(structureFunction);

    ch::nanoseconds totalTDDuration = ch::nanoseconds::zero();
    ch::nanoseconds totalADuration  = ch::nanoseconds::zero();
    double totalTruthDensity        = 0;
    double totalAvailability        = 0;
    auto const probabilities        = generate_probabilities(totalVarCount);

    // Calculation of truth density for each input combination
    for_each_bdd_vars(inputCount, [&](auto const& vars)
    {
        std::vector<teddy::var_cofactor> cofactoredVars;
        cofactoredVars.reserve(1LL << inputCount);
        for (int i = 0; i < inputCount; ++i)
        {
            cofactoredVars.push_back({i, vars[i]});
        }
        tick = std::chrono::high_resolution_clock::now();
        bdd tmpSf = manager.get_cofactor(structureFunction, cofactoredVars);
        tock = std::chrono::high_resolution_clock::now();
        totalADuration += ch::duration_cast<ch::nanoseconds>(tock - tick);

        // Truth density
        double truthDensity = -1;
        tick = std::chrono::high_resolution_clock::now();
        if (totalVarCount < 53)
        {
            truthDensity = static_cast<double>(manager.satisfy_count(1, tmpSf))
                         / static_cast<double>(1LL << (totalVarCount));
        }
        else
        {
            truthDensity = manager.calculate_availability(1, UniformProb(), tmpSf);
        }
        tock = std::chrono::high_resolution_clock::now();
        totalTDDuration += ch::duration_cast<ch::nanoseconds>(tock - tick);
        totalTruthDensity += truthDensity;

        // Availability
        tick = std::chrono::high_resolution_clock::now();
        double const availability = manager.calculate_availability(
            1,
            probabilities,
            tmpSf
        );
        tock = std::chrono::high_resolution_clock::now();
        totalADuration += ch::duration_cast<ch::nanoseconds>(tock - tick);
        totalAvailability += availability;
    });

    double const avgTruthDensity = totalTruthDensity / (1LL << inputCount);
    double const availability    = totalAvailability / (1LL << inputCount);
    ch::nanoseconds const avgTDDuration = totalTDDuration / (1LL << inputCount);
    ch::nanoseconds const avgADuration  = totalADuration / (1LL << inputCount);

    return CircuitResult
    {
        .sfDiagramSize_           = sfDiagramSize,
        .avgTruthDensity_         = avgTruthDensity,
        .availability_            = availability,
        .createDuration_          = createDuration,
        .avgTDensityDuration_     = avgTDDuration,
        .avgAvailabilityDuration_ = avgADuration
    };
}

}