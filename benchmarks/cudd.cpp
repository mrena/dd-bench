#include "cudd.hpp"

#include <chrono>
#include <cuddObj.hh>
#include <iostream>
#include <random>
#include <vector>

namespace b_cudd
{

std::vector<double> generate_probabilities(int const varCount)
{
    std::ranlux48 rng(144);
    std::uniform_real_distribution<double> dist(0.0, 1.0);
    std::vector<double> ps(varCount);
    for (int i = 0; i < varCount; ++i)
    {
        ps[i] = dist(rng);
    }
    return ps;
}

NQueensResult solve_n_queens(std::size_t const n)
{
    long long solutionCount          = -1;
    ch::nanoseconds totalDuration    = ch::nanoseconds::zero();
    ch::nanoseconds satcountDuration = ch::nanoseconds::zero();

    auto const tick = ch::high_resolution_clock::now();
    {
    Cudd manager(n*n,0);
    manager.EnableReorderingReporting();

    std::vector<BDD> board;
    board.reserve(n*n);
    for (std::size_t i = 0; i < n*n; ++i)
    {
        board.push_back(manager.bddVar(i));
    }

    BDD result = manager.bddOne();

    // rows
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            BDD tmp = manager.bddOne();
            for (std::size_t k = 0; k < n; ++k){
                if (j != k)
                {
                    tmp *= !(board[i*n + k]);
                }
            }
            tmp += !(board[i*n + j]);
            result *= tmp;
        }
    }

    // cols
    for (std::size_t j = 0; j < n; ++j)
    {
        for (std::size_t i = 0; i < n; ++i)
        {
            BDD tmp = manager.bddOne();
            for (std::size_t k = 0; k < n; ++k)
            {
                if (i != k)
                {
                    tmp *= !(board[k*n + j]);
                }
            }
            tmp += !(board[i*n + j]);
            result *= tmp;
        }
    }

    // rising diagonals
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            BDD tmp = manager.bddOne();
            for (std::size_t k = 0; k < n; ++k)
            {
                if (j+k >= i && j+k < n+i && k != i)
                {
                    tmp *= !(board[k*n + (j+k-i)]);
                }
            }
            tmp += !(board[i*n + j]);
            result *= tmp;
        }
    }

    // falling diagonals
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            BDD tmp = manager.bddOne();
            for (std::size_t k = 0; k < n; ++k)
            {
                if (j+i >= k && j+i < n+k && k != i)
                {
                    tmp *= !(board[k*n + (j+i-k)]);
                }
            }
            tmp += !(board[i*n + j]);
            result *= tmp;
        }
    }

    // place queens
    for (std::size_t i = 0; i < n; ++i) {
        BDD tmp = manager.bddZero();
        for (std::size_t j = 0; j < n; ++j) {
            tmp += board[i*n + j];
        }
        result *= tmp;
    }
    auto const tickSat = ch::high_resolution_clock::now();
    solutionCount      = Cudd_CountPathsToNonZero(result.getNode());
    auto const tockSat = ch::high_resolution_clock::now();
    satcountDuration   = ch::duration_cast<ch::nanoseconds>(tockSat - tickSat);
    }
    auto const tock = ch::high_resolution_clock::now();

    totalDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    return NQueensResult
    {
        .solutionCount_    = static_cast<long long>(solutionCount),
        .totalDuration_    = totalDuration,
        .satcountDuration_ = satcountDuration
    };
}

CircuitResult solve_circuit(teddy::pla_file const& file)
{
    auto const& plaLines          = file.get_lines();
    long long const inputCount    = file.get_variable_count();
    long long const lineCount     = file.get_line_count();
    long long const functionCount = file.get_function_count();
    long long const totalVarCount = inputCount + lineCount + functionCount;

    Cudd manager(inputCount + lineCount + functionCount, 0, 1 << 20);
    // Cudd manager(inputCount + lineCount + functionCount, 0);

    auto tick = std::chrono::high_resolution_clock::now();
    auto tock = tick;

    std::vector<BDD> inputs;
    std::vector<BDD> andGates;
    std::vector<BDD> orGates;

    int i = 0;
    // Variables for inputs
    for (int k = 0; k < inputCount; ++k)
    {
        inputs.push_back(manager.bddVar(i++));
    }

    // Variables for and gates
    for (int k = 0; k < lineCount; ++k)
    {
        andGates.push_back(manager.bddVar(i++));
    }

    // Variables for or gates
    for (int k = 0; k < functionCount; ++k)
    {
        orGates.push_back(manager.bddVar(i++));
    }

    int nextOrVar = 0;
    BDD failedAnd = manager.bddZero();
    BDD failedOr  = manager.bddOne();

    // BDDs for output functions
    std::vector<BDD> functions;
    // BDDs for output functions including unreliable gates
    std::vector<BDD> functionsRel;
    functions.reserve(functionCount);
    functionsRel.reserve(functionCount);

    for (int fi = 0; fi < functionCount; ++fi)
    {
        int nextAndVar = -1;
        std::vector<BDD> products;
        std::vector<BDD> productsRel;
        products.reserve(lineCount);
        productsRel.reserve(lineCount);

        for (int li = 0; li < lineCount; ++li)
        {
            ++nextAndVar;
            if (plaLines[li].fVals_.get(fi) != 1)
            {
                continue;
            }

            teddy::bool_cube const& cube = plaLines[li].cube_;
            BDD product = manager.bddOne();
            for (int i = 0; i < cube.size(); ++i)
            {
                if (cube.get(i) == 1)
                {
                    product &= inputs[i];
                }
                else if (cube.get(i) == 0)
                {
                    product &= !inputs[i];
                }
            }

            BDD productRel =
                (andGates[nextAndVar] & product) |
                (!andGates[nextAndVar] & failedAnd);

            products.push_back(product);
            productsRel.push_back(productRel);
        }

        if (products.empty())
        {
            products.push_back(manager.bddZero());
        }

        BDD sum = manager.bddZero();
        for (BDD const& product : products)
        {
            sum |= product;
        }

        BDD sumRel =
            (orGates[nextOrVar] & sum) |
            (!orGates[nextOrVar] & failedOr);
        ++nextOrVar;

        functions.push_back(sum);
        functionsRel.push_back(sumRel);
    }

    // BDD for output structure functions
    std::vector<BDD> structureFunctions;
    structureFunctions.reserve(functionCount);
    for (std::size_t i = 0; i < functionCount; ++i)
    {
        structureFunctions.push_back(
            functions[i].Xnor(functionsRel[i])
        );
    }

    // BDD for the final PLA structure function
    BDD structureFunction = manager.bddOne();
    for (BDD const& dd : structureFunctions)
    {
        structureFunction &= dd;
    }

    tock = std::chrono::high_resolution_clock::now();
    auto const createDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    long long const sfDiagramSize = structureFunction.nodeCount();

    ch::nanoseconds totalTDDuration = ch::nanoseconds::zero();
    ch::nanoseconds totalADuration  = ch::nanoseconds::zero();
    double totalTruthDensity        = 0;
    double totalAvailability        = 0;
    auto probabilities              = generate_probabilities(totalVarCount);

    // Calculation of truth density for each input combination
    for_each_bdd_vars(inputCount, [&](auto const& vars)
    {
        BDD mask = manager.bddOne();
        for (int i = 0; i < inputCount; ++i)
        {
            if (vars[i])
            {
                mask &= inputs[i];
            }
            else
            {
                mask &= !inputs[i];
            }
        }

        BDD tmpSf = structureFunction.Restrict(mask);

        // Truth density
        double truthDensity = -1;
        tick = std::chrono::high_resolution_clock::now();
        if (totalVarCount < 53)
        {
            truthDensity = tmpSf.CountMinterm(totalVarCount)
                         / static_cast<double>(1LL << (totalVarCount));
        }
        else
        {
            truthDensity = tmpSf.Correlation(manager.bddOne());
        }
        tock = std::chrono::high_resolution_clock::now();
        totalTDDuration += ch::duration_cast<ch::nanoseconds>(tock - tick);
        totalTruthDensity += truthDensity;

        // Availability
        tick = std::chrono::high_resolution_clock::now();
        double const availability = tmpSf.CorrelationWeights(
            manager.bddOne(),
            probabilities.data()
        );
        tock = std::chrono::high_resolution_clock::now();
        totalADuration += ch::duration_cast<ch::nanoseconds>(tock - tick);
        totalAvailability += availability;
    });

    double const avgTruthDensity = totalTruthDensity / (1LL << inputCount);
    double const availability    = totalAvailability / (1LL << inputCount);
    ch::nanoseconds const avgTDDuration = totalTDDuration / (1LL << inputCount);
    ch::nanoseconds const avgADuration  = totalADuration / (1LL << inputCount);

    return CircuitResult
    {
        .sfDiagramSize_           = sfDiagramSize,
        .avgTruthDensity_         = avgTruthDensity,
        .availability_            = availability,
        .createDuration_          = createDuration,
        .avgTDensityDuration_     = avgTDDuration,
        .avgAvailabilityDuration_ = avgADuration
    };
}

}