#include "buddy.hpp"

#include <libteddy/details/pla_file.hpp>

#include <bdd.h>

#include <cmath>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#include "common.hpp"

namespace b_buddy
{

NQueensResult solve_n_queens(std::size_t const n)
{
    long long solutionCount          = -1;
    ch::nanoseconds totalDuration    = ch::nanoseconds::zero();
    ch::nanoseconds satcountDuration = ch::nanoseconds::zero();

    auto const tick = ch::high_resolution_clock::now();
    {
    bdd_init(1'000'000, 100'000);
    bdd_setvarnum(n*n);
    bdd_setcacheratio(1);
    bdd_setmaxincrease(500'000);

    std::vector<bdd> board;
    board.reserve(n*n);
    for (std::size_t i = 0; i < n*n; ++i)
    {
        board.push_back(bdd_ithvar(i));
    }

    bdd result = bdd_true();

    // rows
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            bdd tmp = bdd_true();
            for (std::size_t k = 0; k < n; ++k){
                if (j != k)
                {
                    tmp = bdd_and(tmp, bdd_not(board[i*n + k]));
                }
            }
            tmp = bdd_or(tmp, bdd_not(board[i*n + j]));
            result = bdd_and(result, tmp);
        }
    }

    // cols
    for (std::size_t j = 0; j < n; ++j)
    {
        for (std::size_t i = 0; i < n; ++i)
        {
            bdd tmp = bdd_true();
            for (std::size_t k = 0; k < n; ++k)
            {
                if (i != k)
                {
                    tmp = bdd_and(tmp, bdd_not(board[k*n + j]));
                }
            }
            tmp = bdd_or(tmp, bdd_not(board[i*n + j]));
            result = bdd_and(result, tmp);
        }
    }

    // rising diagonals
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            bdd tmp = bdd_true();
            for (std::size_t k = 0; k < n; ++k)
            {
                if (j+k >= i && j+k < n+i && k != i)
                {
                    tmp = bdd_and(tmp,bdd_not(board[k*n + (j+k-i)]));
                }
            }
            tmp = bdd_or(tmp, bdd_not(board[i*n + j]));
            result = bdd_and(result, tmp);
        }
    }

    // falling diagonals
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < n; ++j)
        {
            bdd tmp = bdd_true();
            for (std::size_t k = 0; k < n; ++k)
            {
                if (j+i >= k && j+i < n+k && k != i)
                {
                    tmp = bdd_and(tmp, bdd_not(board[k*n + (j+i-k)]));
                }
            }
            tmp = bdd_or(tmp, bdd_not(board[i*n + j]));
            result = bdd_and(result, tmp);
        }
    }

    // place queens
    for (std::size_t i = 0; i < n; ++i) {
        bdd tmp = bdd_false();
        for (std::size_t j = 0; j < n; ++j) {
            tmp = bdd_or(tmp, board[i*n + j]);
        }
        result = bdd_and(result, tmp);
    }

    auto const tickSat = ch::high_resolution_clock::now();
    solutionCount      = bdd_satcount(result);
    auto const tockSat = ch::high_resolution_clock::now();
    satcountDuration   = ch::duration_cast<ch::nanoseconds>(tockSat - tickSat);
    bdd_done();
    }
    auto const tock = ch::high_resolution_clock::now();

    totalDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    return NQueensResult
    {
        .solutionCount_    = static_cast<long long>(solutionCount),
        .totalDuration_    = totalDuration,
        .satcountDuration_ = satcountDuration
    };
}

CircuitResult solve_circuit(teddy::pla_file const& file)
{
    auto const& plaLines          = file.get_lines();
    long long const inputCount    = file.get_variable_count();
    long long const lineCount     = file.get_line_count();
    long long const functionCount = file.get_function_count();
    long long const totalVarCount = inputCount + lineCount + functionCount;

    bdd_init(1'000'000, 100'000);
    bdd_setvarnum(inputCount + lineCount + functionCount);
    bdd_setcacheratio(1);
    bdd_setmaxincrease(500'000);

    auto tick = std::chrono::high_resolution_clock::now();
    auto tock = tick;

    std::vector<bdd> inputs;
    std::vector<bdd> andGates;
    std::vector<bdd> orGates;

    int i = 0;
    // Variables for inputs
    for (int k = 0; k < inputCount; ++k)
    {
        inputs.push_back(bdd_ithvar(i++));
    }

    // Variables for and gates
    for (int k = 0; k < lineCount; ++k)
    {
        andGates.push_back(bdd_ithvar(i++));
    }

    // Variables for or gates
    for (int k = 0; k < functionCount; ++k)
    {
        orGates.push_back(bdd_ithvar(i++));
    }

    int nextOrVar = 0;
    bdd failedAnd = bdd_false();
    bdd failedOr  = bdd_true();

    // BDDs for output functions
    std::vector<bdd> functions;
    // BDDs for output functions including unreliable gates
    std::vector<bdd> functionsRel;
    functions.reserve(functionCount);
    functionsRel.reserve(functionCount);

    for (int fi = 0; fi < functionCount; ++fi)
    {
        int nextAndVar = -1;
        std::vector<bdd> products;
        std::vector<bdd> productsRel;
        products.reserve(lineCount);
        productsRel.reserve(lineCount);

        for (int li = 0; li < lineCount; ++li)
        {
            ++nextAndVar;
            if (plaLines[li].fVals_.get(fi) != 1)
            {
                continue;
            }

            teddy::bool_cube const& cube = plaLines[li].cube_;
            bdd product = bdd_true();
            for (int i = 0; i < cube.size(); ++i)
            {
                if (cube.get(i) == 1)
                {
                    product = bdd_and(product, inputs[i]);
                }
                else if (cube.get(i) == 0)
                {
                    product = bdd_and(product, bdd_not(inputs[i]));
                }
            }

            bdd productRel = bdd_or(
                bdd_and(andGates[nextAndVar], product),
                bdd_and(bdd_not(andGates[nextAndVar]), failedAnd)
            );

            products.push_back(product);
            productsRel.push_back(productRel);
        }

        if (products.empty())
        {
            products.push_back(bdd_false());
        }

        bdd sum = bdd_false();
        for (bdd const& product : products)
        {
            sum = bdd_or(sum, product);
        }

        bdd sumRel = bdd_or(
            bdd_and(orGates[nextOrVar], sum),
            bdd_and(bdd_not(orGates[nextOrVar]), failedOr)
        );
        ++nextOrVar;

        functions.push_back(sum);
        functionsRel.push_back(sumRel);
    }

    // BDD for output structure functions
    std::vector<bdd> structureFunctions;
    structureFunctions.reserve(functionCount);
    for (std::size_t i = 0; i < functionCount; ++i)
    {
        structureFunctions.push_back(
            bdd_biimp(functions[i], functionsRel[i])
        );
    }

    // BDD for the final PLA structure function
    bdd structureFunction = bdd_true();
    for (bdd const& dd : structureFunctions)
    {
        structureFunction = bdd_and(structureFunction, dd);
    }

    tock = std::chrono::high_resolution_clock::now();
    auto const createDuration = ch::duration_cast<ch::nanoseconds>(tock - tick);
    long long const sfDiagramSize = bdd_nodecount(structureFunction);

    ch::nanoseconds totalTDDuration = ch::nanoseconds::zero();
    double totalTruthDensity        = 0;

    // Calculation of truth density for each input combination
    for_each_bdd_vars(inputCount, [&](auto const& vars)
    {
        bdd mask = bdd_true();
        for (int i = 0; i < inputCount; ++i)
        {
            if (vars[i])
            {
                mask = bdd_and(mask, inputs[i]);
            }
            else
            {
                mask = bdd_and(mask, bdd_not(inputs[i]));
            }
        }

        bdd tmpSf = bdd_restrict(
            structureFunction,
            mask
        );

        double truthDensity = -1;
        tick = std::chrono::high_resolution_clock::now();
        if (totalVarCount < 53)
        {
            truthDensity = bdd_satcount(tmpSf)
                         / static_cast<double>(1LL << (totalVarCount));
        }
        else
        {
            double const lga = bdd_satcountln(tmpSf);
            double const lgb = totalVarCount;
            truthDensity     = std::pow(2, lga - lgb);
        }
        tock = std::chrono::high_resolution_clock::now();
        totalTDDuration += ch::duration_cast<ch::nanoseconds>(tock - tick);
        totalTruthDensity += truthDensity;
    });

    bdd_done();

    double const avgTruthDensity = totalTruthDensity / (1LL << inputCount);
    ch::nanoseconds const avgTDDuration = totalTDDuration / (1LL << inputCount);

    return CircuitResult
    {
        .sfDiagramSize_           = sfDiagramSize,
        .avgTruthDensity_         = avgTruthDensity,
        .availability_            = -1,
        .createDuration_          = createDuration,
        .avgTDensityDuration_     = avgTDDuration,
        .avgAvailabilityDuration_ = ch::nanoseconds::zero()
    };
}

}