#ifndef DD_BENCH_COMMON_HPP
#define DD_BENCH_COMMON_HPP

#include <bitset>
#include <cassert>
#include <chrono>
#include <vector>

namespace ch = std::chrono;

struct NQueensResult
{
    long long solutionCount_;
    ch::nanoseconds totalDuration_;
    ch::nanoseconds satcountDuration_;
};

struct CircuitResult
{
    long long sfDiagramSize_;
    double avgTruthDensity_;
    double availability_;
    ch::nanoseconds createDuration_;
    ch::nanoseconds avgTDensityDuration_;
    ch::nanoseconds avgAvailabilityDuration_;
};

template<class F>
void for_each_bdd_vars(long long const varCount, F f)
{
    assert(varCount < 32);
    unsigned long long const Bound = 1ULL << varCount;
    for (unsigned long long vars = 0; vars < Bound; ++vars)
    {
        f(std::bitset<32>(vars));
    }
}

// TODO single template method for all packages
// template<
//     class BDD,
//     class bdd_false,
//     class bdd_true,
//     class bbd_and,
//     class bdd_or,
//     class bdd_biimpl
// >
// CircuitResult solve_circuit_template(teddy::pla_file const& file);

#endif
