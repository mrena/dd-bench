#ifndef DD_BENCH_BUDDY_HPP
#define DD_BENCH_BUDDY_HPP

#include <libteddy/details/pla_file.hpp>
#include "common.hpp"

namespace b_buddy
{
    NQueensResult solve_n_queens(std::size_t const n);
    CircuitResult solve_circuit(teddy::pla_file const& file);
}

#endif