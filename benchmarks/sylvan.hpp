#ifndef DD_BENCH_SYLVAN_HPP
#define DD_BENCH_SYLVAN_HPP

#include <libteddy/details/pla_file.hpp>
#include "common.hpp"

namespace b_sylvan
{
    NQueensResult solve_n_queens(std::size_t const n);
    CircuitResult solve_circuit(teddy::pla_file const& file);
}

#endif